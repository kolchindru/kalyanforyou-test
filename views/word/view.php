<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Word */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Words', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="word-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text',
        ],
    ]) ?>
    <h1>Переводы:</h1>
    <?php foreach ($translations as $item): ?>
            <li>
                <?= Html::encode("{$item->text}") ?>
            </li>
        <?php endforeach; ?>

</div>

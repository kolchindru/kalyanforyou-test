<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "translations".
 *
 * @property int $id
 * @property int $word_id
 * @property int $language_id
 */
class Translation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'translations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['word_id', 'language_id'], 'required'],
            [['word_id', 'language_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'word_id' => 'Word ID',
            'language_id' => 'Language ID',
        ];
    }
}

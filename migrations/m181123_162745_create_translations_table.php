<?php

use yii\db\Migration;

/**
 * Handles the creation of table `translations`.
 */
class m181123_162745_create_translations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('translations', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('translations');
    }
    public function up()
    {
        $tableOptions = null;
 
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
 
        $this->createTable('translations', [
            'id' => $this->primaryKey(),
            'word_id' => $this->integer()->notNull(),
            'language_id' => $this->integer()->notNull(),
        ], $tableOptions);
    }
 
    public function down()
    {
        $this->dropTable('translations');
    }
}

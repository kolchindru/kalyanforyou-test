<?php

use yii\db\Migration;

/**
 * Handles the creation of table `word`.
 */
class m181121_103605_create_word_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('word', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('word');
    }
    public function up()
    {
        $tableOptions = null;
 
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
 
        $this->createTable('word', [
            'id' => $this->primaryKey(),
            'text' => $this->string()->notNull()->unique(),
        ], $tableOptions);
    }
 
    public function down()
    {
        $this->dropTable('word');
    }
}
